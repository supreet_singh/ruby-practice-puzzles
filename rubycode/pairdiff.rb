#find number of pairs in arr with difference k

def pdiff(arr,k)
  a= arr.combination(2).to_a
  a.select! {|i| ((i[0] - i[1]) == k || (i[1] - i[0]) == k)}
  a.length
end

puts pdiff([363374326, 364147530, 61825163,
   1073065718, 1281246024, 1399469912, 428047635,
  491595254, 879792181, 1069262793],1)
