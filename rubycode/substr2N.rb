=beginComplete the function getEqualSumSubstring, which takes a single argument.
The single argument is a string s, which contains only non-zero digits.
This function should print the length of longest contiguous substring of s,
such that the length of the substring is 2*N digits and the sum of the
leftmost N digits is equal to the sum of the rightmost N digits.If there
is no such string, your function should print 0.
=end
def substr2N(str)
  a = Array.new
  x= str.split("")
  (0..((x.length)-1)).each {|i|

    ((i+1)..((x.length) - 1)).step(2) {|j|
      a.push(x[i..j].join(""))
    }

  }

  longest = 0
  a.each {|i| x= i.split("")
    if sum(x[0..((i.length)/2 -1)]) == sum(x[(i.length/2)..((i.length) -1)])
        if i.length > longest
          longest = i.length
        end
      end
    }

    longest

end

def sum(arr)
  sum =0
  arr.each{|i| sum += (i.to_i)}
  sum
end
puts substr2N("986561517416921217551395112859219257312")
